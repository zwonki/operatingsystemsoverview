# Operating Systems Overview in Automotive Solutions

[TOC]

## 1 Autosar
AUTomotive Open System ARchitecture (AUTOSAR) is a development partnership of automotive interested parties founded in 2003. It pursues the objective to create and establish an open and standardized software architecture for automotive electronic control units (ECUs). Goals include the scalability to different vehicle and platform variants, transferability of software, the consideration of availability and safety requirements, a collaboration between various partners, sustainable use of natural resources, and maintainability during the product lifecycle

### 1.1 Classic
One essential concept is the virtual functional bus (VFB). This virtual bus decouples the applications from the infrastructure. It communicates via dedicated ports, which means that the communication interfaces of the application software must be mapped to these ports. The VFB handles communication both within the individual ECU and between ECUs. From an application point of view, no detailed knowledge of lower-level technologies or dependencies is required. This supports hardware-independent development and usage of application software.

The AUTOSAR layered architecture is offering all the mechanisms needed for software and hardware independence. It distinguishes between three main software layers which run on a Microcontroller (µC): application layer, runtime environment (RTE), and basic software (BSW).
![](img/AR.png)
The applications of the different automotive domains interface the basic software by means of the RTE.
![](img/AR_detail.png)
In addition to defining architecture and interfaces, AUTOSAR also defines a methodology which enables the configuration of the complete AUTOSAR stack and enhances interoperability between different tool chains. On the one hand this is important for the collaboration within development projects and on the other hand this is important to cut down development costs.
![](img/vbf.png)
<https://www.autosar.org/standards/classic-platform/>

### 1.2 Adaptive
Adaptive Platform (AP). AP provides mainly high-performance computing and communication mechanisms and offers flexible software configuration, e.g., to support software update over-the-air. Features specifically defined for the CP, such as access to electrical signals and automotive specific bus systems, can be integrated into the AP.
![](img/ARA.png)
<https://www.autosar.org/standards/adaptive-platform/>

### 1.3 Classic vs Adaptive
Comparison of the Autosar types. Pointing main differences with the focus on the underling OS.

## 2 What is and why you need Operating System
Basic introduction to the OS concepts.

### 2.1 Microcontroller vs Microprocessor
A microprocessor is a computer processor where the data processing logic and control is included on a single integrated circuit, or a small number of integrated circuits. 

The microprocessor contains the arithmetic, logic, and control circuitry required to perform the functions of a computer's central processing unit. The integrated circuit is capable of interpreting and executing program instructions and performing arithmetic operations. 
![](img/Hiroshige.Goto.png)
The microprocessor is a multipurpose, clock-driven, register-based, digital integrated circuit that accepts binary data as input, processes it according to instructions stored in its memory, and provides results (also in binary form) as output. Microprocessors contain both combinational logic and sequential digital logic and operate on numbers and symbols represented in the binary number system. 

A microcontroller can be considered a self-contained system with a processor, memory and peripherals and can be used as an embedded system. The majority of microcontrollers in use today are embedded in other machinery, such as automobiles, telephones, appliances, and peripherals for computer systems. 
![](img/AURIX_BD.png)
Microcontrollers must provide real-time (predictable, though not necessarily fast) response to events in the embedded system they are controlling. When certain events occur, an interrupt system can signal the processor to suspend processing the current instruction sequence and to begin an interrupt service routine (ISR, or "interrupt handler") which will perform any processing required based on the source of the interrupt, before returning to the original instruction sequence.

Typically, uC are based on modified Harvard architecture.

An SoC integrates a microcontroller, microprocessor or perhaps several processor cores with peripherals like a GPU, Wi-Fi and cellular network radio modems, and/or one or more coprocessors. Like how a microcontroller integrates a microprocessor with peripheral circuits and memory, an SoC can be seen as integrating a microcontroller with even more advanced peripherals. For an overview of integrating system components, see system integration.


#### 2.1.1 MMU vs MPU
A memory management unit is a computer hardware unit having all memory references passed through itself, primarily performing the translation of virtual memory addresses to physical addresses.

An MMU effectively performs virtual memory management, handling at the same time memory protection, cache control, bus arbitration and, in simpler computer architectures (especially 8-bit systems), bank switching.

![](img/MMU_principle_updated.png)

<https://en.wikipedia.org/wiki/Memory_management_unit>

In computing, virtual memory, or virtual storage is a memory management technique that provides an „idealized abstraction of the storage resources that are actually available on a given machine” which „creates the illusion to users of a very large (main) memory”.

<https://en.wikipedia.org/wiki/Virtual_memory>

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.sys_arch/topic/proc_memmgr.html>

![](img/Virtual_address_space_and_physical_address_space_relationship.svg)

In computing, an input–output memory management unit (IOMMU) is a memory management unit (MMU) that connects a direct-memory-access–capable (DMA-capable) I/O bus to the main memory. 

<https://en.wikipedia.org/wiki/Input%E2%80%93output_memory_management_unit>

![](img/MMU_and_IOMMU.svg)

Like a traditional MMU, which translates CPU-visible virtual addresses to physical addresses, the IOMMU maps device-visible virtual addresses to physical addresses.

A memory protection unit (MPU), is a computer hardware unit that provides memory protection. It is usually implemented as part of the central processing unit (CPU). MPU is a trimmed down version of memory management unit (MMU) providing only memory protection support. It is usually implemented in low power processors that require only memory protection and do not need the full-fledged feature of a memory management unit like virtual memory management

<https://en.wikipedia.org/wiki/Memory_protection_unit>


#### 2.1.2 System on a Chip
An SoC integrates a microcontroller, microprocessor or perhaps several processor cores with peripherals like a GPU, Wi-Fi and cellular network radio modems, and/or one or more coprocessors. Like how a microcontroller integrates a microprocessor with peripheral circuits and memory, an SoC can be seen as integrating a microcontroller with even more advanced peripherals. For an overview of integrating system components, see system integration.

<https://en.wikipedia.org/wiki/System_on_a_chip>

![](img/r-car-h3-block-diagram.jpg)

#### 2.1.3 Safety islands
The promise of a safety island is to manage and control the safety content inside an SoC by:

* Signaling failures
* Enabling recovery
* Adapting to future needs

Preventing any harm from the failure of a safety-critical system is what functional safety is all about. Adding logic inside a chip to detect random hardware faults that are either latent or transient is part of diagnostics coverage, while meeting the desired level of ASIL

### 2.2 What is Kernel
Introduction to the OS Kernel functionalities. The tasks and types of the Kernel.

An operating system (OS) is system software that manages:

* computer hardware,
* software resources,  
* provides common services for computer programs,
* access control.

<https://en.wikipedia.org/wiki/Kernel_(operating_system)>

<https://kernel.org/>

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.sys_arch/topic/kernel.html>

With the aid of the firmware and device drivers, the kernel provides the most basic level of control over all the computer's hardware devices.

It manages memory access for programs in the RAM, it determines which programs get access to which hardware resources, it sets up or resets the CPU's operating states for optimal operation at all times, and it organizes the data for long-term non-volatile storage with file systems on such media as disks, tapes, flash memory, etc.

Kernel is running in the „most” privileged ring.

<https://en.wikipedia.org/wiki/Protection_ring>

<https://developer.arm.com/documentation/ddi0488/d/programmers-model/armv8-architecture-concepts/exception-levels>

![x86 priviliged rings](img/Priv_rings.svg)

![ARM Exception Levels](img/arm_els.png)

It is called kernel space or kernel mode unlike the user space or user mode which runs in the least privileged ring.

The code in kernel space can access all the devices.

Jump from one to another is called context switch and costs execution time, as the CPU must safely switch its state.

<https://en.wikipedia.org/wiki/System_call>

<https://www.linux.it/~rubini/docs/ksys/>

<https://man7.org/linux/man-pages/man2/syscalls.2.html>

![](img/system-call.png)

Kernels can be distinguished based on their size:

![](img/OS-structure2.svg)

The monolithic kernels tends to grow, just like Linux does:

![](img/linux-kernel-map.jpeg)

#### 2.2.1 Process
In computing, a process is the instance of a computer program that is being executed by one or many threads. It contains the program code and its activity. 

![](img/process.png)

While a computer program is a passive collection of instructions, a process is the actual execution of those instructions.

<https://en.wikipedia.org/wiki/Process_(computing)>

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.prog/topic/process.html>

Processes are identified by the Process ID (PID).

<https://en.wikipedia.org/wiki/Process_identifier>

Process isolation is a set of different hardware and software technologies designed to protect each process from other processes on the operating system. It does so by preventing process A from writing to process B.

It is (should be) fundamental. One process cannot and shall not try to access other process memory.

Processes can be scheduled in to basic ways:

* Cooperative multitasking – not recomended
* Preemptive multitasking – recomended
  * Real-Time schedulers


Cooperative multitasking, also known as non-preemptive multitasking, is a style of computer multitasking in which the operating system never initiates a switch from a running process to another process. Instead, processes voluntarily yield control periodically or when idle or logically blocked in order to enable multiple applications to be run concurrently.

This a way how the DOS and Windows 95 was operating. We really do not want to repeat those experience.

Preemptive multitasking involves the use of an interrupt mechanism which suspends the currently executing process and invokes a scheduler to determine which process should execute next. Therefore, all processes will get some amount of CPU time at any given time.

Real Time systems are the subset of the preemptive systems.

A scheduler is what carries out the scheduling activity.

<https://en.wikipedia.org/wiki/Scheduling_(computing)#SCHEDULER>

<https://www.kernel.org/doc/html/latest/scheduler/index.html>

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.prog/topic/overview_PRIOR.html>

Schedulers are often implemented so they keep all computer resources busy (as in load balancing), allow multiple users to share system resources effectively, or to achieve a target quality of service.

Scheduling is fundamental to computation itself, and an intrinsic part of the execution model of a computer system; the concept of scheduling makes it possible to have computer multitasking with a single central processing unit (CPU).

Scheduling disciplines are algorithms used for distributing resources among parties which simultaneously and asynchronously request them.

The main purposes of scheduling algorithms are to minimize resource starvation and to ensure fairness among the parties utilizing the resources. Scheduling deals with the problem of deciding which of the outstanding requests is to be allocated resources. There are many different scheduling algorithms.

#### 2.2.2 System calls

In computing, a system call (commonly abbreviated to syscall) is the programmatic way in which a computer program requests a service from the kernel of the operating system on which it is executed. This may include hardware-related services (for example, accessing a hard disk drive), creation and execution of new processes, and communication with integral kernel services such as process scheduling. System calls provide an essential interface between a process and the operating system.

In most systems, system calls can only be made from userspace processes, while in some systems, OS/360 and successors for example, privileged system code also issues system calls.

#### 2.2.3 IPC
As the processes cannot share memory and should not know about each other the Kernel needs to support the way of communication among processes.

In general, this is called the Inter Process Communication.

The most common IPC methods are:

1. Shared File
2. Shared memory
3. Pipes
4. Signals
5. Sockets


### 2.3 What is UNIX, how it started
Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, Brian Kernighan and others.

<https://en.wikipedia.org/wiki/Unix>

<https://en.wikipedia.org/wiki/History_of_Unix>

Unix systems are characterized by a modular design that is sometimes called the "Unix philosophy". According to this philosophy, the operating system should provide a set of simple tools, each of which performs a limited, well-defined function. A unified file system (the Unix file system) and an inter-process communication mechanism known as "pipes" serve as the main means of communication, and a shell scripting and command language (the Unix shell) is used to combine the tools to perform complex workflows.

Unix distinguishes itself from its predecessors as the first portable operating system: almost the entire operating system is written in the C programming language, which allows Unix to operate on numerous platforms.

Software Development in Unix follows few simple rules called the Unix Philosophy.

<https://en.wikipedia.org/wiki/Unix_philosophy>

Make each program do one thing well. To do a new job, build afresh rather than complicate old programs by adding new "features".

Expect the output of every program to become the input to another, as yet unknown, program. Don't clutter output with extraneous information. Avoid stringently columnar or binary input formats. Don't insist on interactive input.

Design and build software, even operating systems, to be tried early, ideally within weeks. Don't hesitate to throw away the clumsy parts and rebuild them.

Use tools in preference to unskilled help to lighten a programming task, even if you have to detour to build the tools and expect to throw some of them out after you've finished using them.

![](img/unix_history.svg)

<https://en.wikipedia.org/wiki/Unix_wars>

### 2.4 What is UNIX-like system
Unix-like (sometimes referred to as UN*X or *nix) operating system is one that behaves in a manner similar to a Unix system, while not necessarily conforming to or being certified to any version of the Single UNIX Specification. A Unix-like application is one that behaves like the corresponding Unix command or shell. There is no standard for defining the term, and some difference of opinion is possible as to the degree to which a given operating system or application is "Unix-like".

The term can include free and open-source operating systems inspired by Bell Labs' Unix or designed to emulate its features, commercial and proprietary work-alikes, and even versions based on the licensed UNIX source code (which may be sufficiently "Unix-like" to pass certification and bear the "UNIX" trademark).

Linux, vxWorks and QNX are Unix-like Systems. QNX is more Unix-ly. vxWorks is less Unix-ly. None of them is Unix.

Through the years Unix created set of the requirments which fullfilmet is neccessary to be called Unix. The documentation can be download from: <https://publications.opengroup.org/g101> . Main chapters are:

Chapter 1 Describes how the Single UNIX Specification environment is organized and where to look for more detailed information in the Single UNIX Specification.

Chapter 2 Gives an overview of the documents that comprise the different parts of the Single UNIX Specification, Version 4, and how they are organized. This includes an overview of the contents of each of the documents and their relation to each other.

Chapter 3 Covers the C language programming interfaces that are the system interfaces, describing the  base  documents, an overview of the  changes  in  this  version,  and  lists  of  the  new functions. It also  describes  the  options  and  groups  the  interfaces  into  categories  to  give the  reader  an understanding of what facilities are available.

Chapter 4 Covers the shell and utilities, describing the base documents, an overview of the changes in  this  version,  and  lists  of  the  new features. It also  contains  rationale  of  why certain utilities  are excluded, and also a description of the options.

Chapter 5 Covers the C language headers and name space rules. It describes the base documents and gives an overview of the changes in this version.

Chapter 6 Describes the contents of X/Open Curses, Issue 7.

#### 2.4.1 GNU snd Software licensing

![](img/licenses.png)

<https://en.wikipedia.org/wiki/Free-software_license>

<https://en.wikipedia.org/wiki/GNU_General_Public_License>

<https://choosealicense.com/>

## 3 Basic OS Components
What else do you need for the operating system to be useful.

### 3.1 POSIX, why we (do not) need it
The OS standardization and how it can feel outdated and not always suited for Automotive purposes.
The Portable Operating System Interface (POSIX) is a family of standards specified by the IEEE Computer Society for maintaining compatibility between operating systems.

<https://en.wikipedia.org/wiki/POSIX>

<https://standards.ieee.org/ieee/1003.1/7700/>

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.sys_arch/topic/intro_Why_POSIX.html>

POSIX defines the application programming interface (API), along with command line shells and utility interfaces, for software compatibility with variants of Unix and other operating systems.

POSIX is huge and forged in the 1980s. Usually OS supports only subset of it.

PSE51 and PSE52 are the POSIX subset designed for the Real Time operating systems. 

* PSE51 focuses on the thread synchronization. Does not required multiprocessing. It was chosen as a base for the AA development.
* PSE52 extends PSE51 on the file manipulation.

<https://ieeexplore.ieee.org/document/798785>

<https://ieeexplore.ieee.org/document/1342418>

libc implements POSIX. It is Released under the GNU Lesser General Public License, glibc is free software. glibc provides the functionality required by the Single UNIX Specification, POSIX (1c, 1d, and 1j) and some of the functionality required by ISO C11, ISO C99, Berkeley Unix (BSD) interfaces, the System V Interface Definition (SVID) and the X/Open Portability Guide (XPG), Issue 4.2, with all extensions common to XSI (X/Open System Interface) compliant systems along with all X/Open UNIX extensions.


### 3.2 About users and groups
Access control in the system.

User is a “person” who utilizes the service.

Unix-like operating systems identify a user by a value called a user identifier, often abbreviated to user ID or UID. The UID, along with the group identifier (GID) and other access control criteria, is used to determine which system resources a user can access.

<https://en.wikipedia.org/wiki/User_(computing)>

<https://en.wikipedia.org/wiki/User_identifier>

The password file maps textual usernames to UIDs. UIDs are stored in the inodes of the Unix file system, running processes, tar archives, and the now-obsolete Network Information Service.

In Unix-like systems, multiple users can be put into groups.

<https://en.wikipedia.org/wiki/Group_(computing)>

POSIX and conventional Unix file system permissions are organized into three classes, user, group, and others.

The use of groups allows additional abilities to be delegated in an organized fashion, such as access to disks, printers, and other peripherals.

In computing, the superuser is a special user account used for system administration. Depending on the operating system (OS), the actual name of this account might be root, administrator, admin or supervisor.

<https://en.wikipedia.org/wiki/Superuser>

In some cases, the actual name of the account is not the determining factor; on Unix-like systems, for example, the user with a user identifier (UID) of zero is the superuser, regardless of the name of that account; and in systems which implement a role-based security model, any user with the role of superuser (or its synonyms) can carry out all actions of the superuser account.

### 3.3 Device Drivers and Resource Managers
How the OS can communicate with the hardware. Comparison of monolithic and micro kernel approaches.

![](img/ldd_vs_qnx_rm.png)

Basic Device Drivers Types:
* Character devices
* Block Devices
* Network Interfaces
* Filesystems

To minimize the runtime memory requirements of the final system, and to cope with the wide variety of devices that may be found in a custom embedded system, the QNX allows user-written processes to act as resource managers that can be started and stopped dynamically.

Resource managers are typically responsible for presenting an interface to various types of devices. This may involve managing actual hardware devices (like serial ports, parallel ports, network cards, and disk drives) or virtual devices (like /dev/null, a network filesystem, and pseudo-ttys).

A resource manager is basically a user-level server program that accepts messages from other programs and, optionally, communicates with hardware. The QNX native IPC services allow the resource manager to be decoupled from the OS.

Linux Device Drivers: 

<https://lwn.net/Kernel/LDD3/>

QNX Resource Managers: 

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.sys_arch/topic/resource.html>

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.getting_started/topic/s1_resmgr.html>

#### 3.3.1 Character Devices
A character (char) device is one that can be accessed as a stream of bytes (like a file); a char driver is in charge of implementing this behavior. Such a driver usually implements at least the:

* Open,
* Close,
* Read,
* Write

system calls. The text console (/dev/console) and the serial ports (/dev/ttyS0 and friends) are examples of char devices, as they are well represented by the stream abstraction. Char devices are accessed by means of filesystem nodes, such as /dev/tty1 and /dev/lp0. The only relevant difference between a char device and a regular file is that you can always move back and forth in the regular file, whereas most char devices are just data channels, which you can only access sequentially. There exist, nonetheless, char devices that look like data areas, and you can move back and forth in them; for instance, this usually applies to frame grabbers, where the applications can access the whole acquired image using mmap or lseek.

<https://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.sys_arch/topic/char.html>

#### 3.3.2 Block Devices
Like char devices, block devices are accessed by filesystem nodes in the /dev directory. A block device is a device (e.g., a disk) that can host a filesystem. In most Unix systems, a block device can only handle I/O operations that transfer one or more whole blocks, which are usually 512 bytes (or a larger power of two) bytes in length. In reality block and char devices differ only in the way data is managed internally by the kernel, and thus in the kernel/driver software interface. Like a char device, each block device is accessed through a filesystem node, and the difference between them is transparent to the user. Block drivers have a completely different interface to the kernel than char drivers.

#### 3.3.3 Network Interfaces
Any network transaction is made through an interface, that is, a device that is able to exchange data with other hosts. Usually, an interface is a hardware device, but it might also be a pure software device, like the loopback interface. A network interface oversees sending and receiving data packets, driven by the network subsystem of the kernel, without knowing how individual transactions map to the actual packets being transmitted. Many network connections(especially those using TCP) are stream-oriented, but network devices are, usually, designed around the transmission and receipt of packets. A network driver knows nothing about individual connections; it only handles packets.

Not being a stream-oriented device, a network interface isn’t easily mapped to a node in the filesystem, as /dev/tty1 is. The Unix way to provide access to interfaces is still by assigning a unique name to them (such as eth0), but that name doesn’t have a corresponding entry in the filesystem. Communication between the kernel and a network device driver is completely different from that used with char and block drivers. Instead of read and write, the kernel calls functions related to packet transmission.

<https://www.qnx.com/developers/docs/7.1/#com.qnx.doc.core_networking/topic/drivers.html>

#### 3.3.4 Filesystems
In computing, a file system or filesystem (often abbreviated to fs) controls how data is stored and retrieved. Without a file system, data placed in a storage medium would be one large body of data with no way to tell where one piece of data stops and the next begins. By separating the data into pieces and giving each piece a name, the data is easily isolated and identified. Taking its name from the way paper-based data management system is named, each group of data is called a "file." The structure and logic rules used to manage the groups of data and their names is called a "file system."

<https://en.wikipedia.org/wiki/File_system>

<https://en.wikipedia.org/wiki/Journaling_file_system>

<https://en.wikipedia.org/wiki/Ext4>

<https://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.sys_arch/topic/fsys.html>

### 3.4 Unix-like Systems Directory Structure
/ – With (root) / at the base of the file system and all other directory spreading from there.

This is the root directory which should contain only the directories needed at the top level of the file structure.

The “root” may exist only in RAM and never be truly present on any physical media.

* /bin – It contains both admin and non-admin executable commands, for example, some of the commands like touch, mkdir, top, kill etc….
* /sbin (Superuser binary Dir) – It holds only the root user executable commands, for example, commands like shutdown, reboot,fdisk.
* /etc – This directory holds the entire server configuration files, like user related files, network related files, booting files and much more.If you need any configuration files you can get that from the /etc directory.
  1. To check the user properties you would need to open the config file /etc/passwd (passwd is a config file which contains all the user properties)…..
  2. If I want to add a remote server IP for name resolving purpose, I would need to open the network config file /etc/hosts(inside hosts file we will put the remote server IP address).
* /dev (Device)- This Directory contains all the device files and the logical name of all the hardware devices that are connected to the server.
What is meant by logical name of a device?<br/>
For example, if I give you a fresh hard-disk in your hand how you would call that drive? Technically if you want to say that means, you have to say it’s a raw disk (i.e, A disk without any partition).<br/>
Once you divided the disk in to individual partitions user can access that hard-disk by the partition numbers (example in windows you would use c:, d:, e:….) this c,d,e drive letters are called as a logical name of the hard disk.<br/>
Like this, you will have names to access the hardware that is connected to the server. If a user needs to access any hardware he must have to use the logical names. Ex: For harddisk:* /dev/sda (sda–if it is a SCSI disk)<br/>
  1. /dev/hde (hde–if it is an IDE disk)
  2. /dev/mmc  (mmc drive)<br/>
For tape-drive /dev/rmt0 (“rmt” means remote magnetic tape and 0 is tape no.1)
* /proc (Process) - This directory contains all the ongoing process information and also it holds the hardware configuration details of your server.
* /var (Variable) - This is an important directory which contains all the server related log files, the path for the log files in /var/log inside this directory path you will have different types of individual log files.
* /home - This is the default home directory for all normal user logins. you may have a question pops up in your mind why the home directory is required?
When a user logged in to the system all the initialization files which are required to create the user environment or desktop will load from your default home directory
* /root-This is the default home directory for root login(the same like /home dir).
* /usr - This directory contains all the executable files, programs, scripts that are run by all users. As we know the /bin and /sbin directory which contains normal user and root user commands, now the executable path for all the commands start from either /usr/bin or /usr/sbin when you run any commands say for instance, “touch” (which is used to create an empty file), the shell will receive the command from the keyboard and the path for this command is /usr/bin/touch.
* /boot - This directory contains all the booting files that are needed when you power on the server.
* /mnt (Mount) - It contains all the currently mounted filesystem details (Mounting and unmounting I will explain in our Linux topic)
* /media - Default mount point directory for the removable media like USB, CD-ROM etc. when you insert a CD or DVD in your Linux box the CD will get automatically mounted inside the /media directory,from this directory you can access all the data from the CD.

![](img/FS-layout.png)

<https://tldp.org/LDP/Linux-Filesystem-Hierarchy/html/c23.html> 

A virtual file system (VFS) or virtual filesystem switch is an abstract layer on top of a more concrete file system.

![](img/vfs.png)

The purpose of a VFS is to allow client applications to access different types of concrete file systems in a uniform way. A VFS can, for example, be used to access local and network storage devices transparently without the client application noticing the difference.

It can be used to bridge the differences in Windows, classic Mac OS/macOS and Unix filesystems, so that applications can access files on local file systems of those types without having to know what type of file system they are accessing.

## 4 Linux Distributions
A Linux distribution (often abbreviated as distro) is an operating system made from a software collection that is based upon the Linux kernel and, often, a package management system.

Linux distributions are wide variety of systems ranging from embedded devices (e. g. OpenWrt) and personal computers (e. g. Debian, Red Hat, Ubuntu, Linux Mint) to powerful supercomputers (e,g, Rocks Cluster Distribution).

<https://en.wikipedia.org/wiki/Linux_distribution>

<https://distrowatch.com/>

### 4.2 Packet Managers
A package manager or package-management system is a collection of software tools that automates the process of installing, upgrading, configuring, and removing computer programs for a computer's operating system in a consistent manner. All distributions have the dedicated PM, e.g.:

* APT (Debian)
* YUM (Red Hat)
* SNAP (mostly Ubuntu, but almost all)
* Portage (Gentoo)

### 4.1 shell 
In computing, a shell is a computer program which exposes an operating system's services to a human user or other program. In general, operating system shells use either a command-line interface (CLI) or graphical user interface (GUI), depending on a computer's role and particular operation. It is named a shell because it is the outermost layer around the operating system.

Most known shells:

* Bash
* KSH
* SH
* ZSH

### 4.1 Desktop vs Embedded
In the embedded world you need to build your own distribution!

### 4.2 Linux From the Scratch
Is a type of a Linux installation and the name of a book written by Gerard Beekmans, and as of May 2021, mainly maintained by Bruce Dubbs. The book gives readers instructions on how to build a Linux system from source. The book is available freely from the Linux From Scratch site.

This name is commonly used in the context of the self made distribution. With or without tooling help.
<https://www.linuxfromscratch.org/>

The Yocto Project is a Linux Foundation collaborative open-source project whose goal is to produce tools and processes that enable the creation of Linux distributions for embedded and IoT software that are independent of the underlying architecture of the embedded hardware. The project was announced by the Linux Foundation in 2010 and launched in March 2011, in collaboration with 22 organizations, including OpenEmbedded.
<https://www.yoctoproject.org/>

BitBake is a make-like build tool with the special focus of distributions and packages for embedded Linux cross compilation, although it is not limited to that. It is inspired by Portage, which is the package management system used by the Gentoo Linux distribution. BitBake existed for some time in the OpenEmbedded project until it was separated out into a standalone, maintained, distribution-independent tool. BitBake is co-maintained by the Yocto Project and the OpenEmbedded project.
<https://www.yoctoproject.org/docs/1.6/bitbake-user-manual/bitbake-user-manual.html>

### 4.3 Android
Android is a mobile operating system based on a modified version of the Linux kernel and other open-source software, designed primarily for touchscreen mobile devices such as smartphones and tablets. Android is developed by a consortium of developers known as the Open Handset Alliance and commercially sponsored by Google.

It is free and open-source software; its source code is known as Android Open-Source Project (AOSP), which is primarily licensed under the Apache License. However most Android devices ship with additional proprietary software pre-installed, most notably Google Mobile Services (GMS) which includes core apps such as Google Chrome, the digital distribution platform Google Play, and associated Google Play Services development platform. 

The source code for Android is open-source: it is developed in private by Google, with the source code released publicly when a new version of Android is released. Google publishes most of the code (including network and telephony stacks) under the non-copyleft Apache License version 2.0. which allows modification and redistribution. The license does not grant rights to the "Android" trademark, so device manufacturers and wireless carriers have to license it from Google under individual contracts. Associated Linux kernel changes are released under the copyleft GNU General Public License version 2, developed by the Open Handset Alliance, with the source code publicly available always.

Only the base Android operating system (including some applications) is open-source software, whereas most Android devices ship with a substantial amount of proprietary software, such as Google Mobile Services, which includes applications such as Google Play Store, Google Search, and Google Play Services – a software layer that provides APIs for the integration with Google-provided services, among others. These applications must be licensed from Google by device makers and can only be shipped on devices which meet its compatibility guidelines and other requirements.

Richard Stallman and the Free Software Foundation have been critical of Android and have recommended the usage of alternatives such as Replicant, because drivers and firmware vital for the proper functioning of Android devices are usually proprietary, and because the Google Play Store application can forcibly install or uninstall applications and, as a result, invite non-free software. In both cases, the use of closed-source software causes the system to become vulnerable to backdoors.

It has been argued that because developers often require to purchase the Google-brand Android license, this has turned the theoretically open system into a freemium service.

<https://en.wikipedia.org/wiki/Android_(operating_system)>

#### 4.3.1 Stack
Android stack delivers all system layer needed for effective embedded system. 

Binder IPC. The Binder Inter-Process Communication (IPC) mechanism allows the application framework to cross process boundaries and call into the Android system services code. This enables high level framework APIs to interact with Android system services. At the application framework level, this communication is hidden from the developer and things appear to "just work". 

System services. System services are modular, focused components such as Window Manager, Search Service, or Notification Manager. Functionality exposed by application framework APIs communicates with system services to access the underlying hardware. Android includes two groups of services: system (such as Window Manager and Notification Manager) and media (services involved in playing and recording media). 

Hardware abstraction layer (HAL). A HAL defines a standard interface for hardware vendors to implement, which enables Android to be agnostic about lower-level driver implementations. Using a HAL allows you to implement functionality without affecting or modifying the higher-level system. HAL implementations are packaged into modules and loaded by the Android system at the appropriate time. For details, see Hardware Abstraction Layer (HAL). 

Linux kernel. Developing your device drivers is similar to developing a typical Linux device driver. Android uses a version of the Linux kernel with a few special additions such as Low Memory Killer (a memory management system that is more aggressive in preserving memory), wake locks (a PowerManager system service), the Binder IPC driver, and other features important for a mobile embedded platform. These additions are primarily for system functionality and do not affect driver development. You can use any version of the kernel as long as it supports the required features (such as the binder driver). However, we recommend using the latest version of the Android kernel. For details, see Building Kernels. 

<https://source.android.com/devices/architecture?hl=en>

<https://developer.android.com/guide/platform#art>

#### 4.3.2 OTA
Android devices in the field can receive and install over-the-air (OTA) updates to the system, application software, and time zone rules. This section describes the structure of update packages and the tools provided to build them. It is intended for developers who want to make OTA updates work on new Android devices and those who want to build update packages for released devices. 

OTA updates are designed to upgrade the underlying operating system, the read-only apps installed on the system partition, and/or time zone rules; these updates do not affect applications installed by the user from Google Play. 

Modern Android devices have two copies of each partition (A and B) and can apply an update to the currently unused partition while the system is running but idle. A/B devices do not need space to download the update package because they can apply the update as they read it from the network; this is known as streaming A/B. 

<https://source.android.com/devices/tech/ota?hl=en>

## 5 QNX
QNX is a commercial Unix-like real-time operating system, aimed primarily at the embedded systems market. QNX was one of the first commercially successful microkernel operating systems. As of 2020, it is used in a variety of devices including cars and mobile phones.

QNX and Linux are both Unix-like. They both do support POSIX. That is, it. Nothing more in common. 

### 5.1 Neutrino Microkernel and QNX Embedded
The QNX kernel, procnto, contains only CPU scheduling, interprocess communication, interrupt redirection and timers.  Everything else runs as a user process. This is made possible by two key mechanisms: subroutine-call type interprocess communication, and a boot loader which can load an image containing the kernel and any desired set of user programs and shared libraries. There are no device drivers in the kernel. The network stack is based on NetBSD code. Along with its support for its own, native, device drivers, QNX supports its legacy, io-net manager server, and the network drivers ported from NetBSD.

For each supported SoC/Chip QNX delivers the Board Support Package. It consists of:

* QNX IPL
* The set of precompiled libraries for the target
* Default image template
* The (not certified) source code of the bootstrap process and drivers which may be extended by the user.

You can download the BSP via the Software Center. BSP is delivered as the zip file. The archive includes the folder structure which will be helpful in the future development. To perform the compilation run make command. Usually, the last step of compilation is image build with the mkifs recipe. The result is an image ready to be run.

Most of the IPC available in the QNX is POSIX compliant e.g.:

* Signals
* Shared Memory
* Unnamed and named pipes
* POSIX Message Queues

QNX adds one custom IPC construct:

* Message passing

<https://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.sys_arch/topic/ipc.html>

Message passing is default way of communication for QNX Resource Managers (drivers).

Most of the documentation is open:

<http://www.qnx.com/developers/docs/6.5.0/>

<http://www.qnx.com/developers/docs/7.0.0/>

<http://www.qnx.com/developers/docs/7.1/>  

### 5.2 ASIL-D ?
QNX Neutrino is ASIL-D certified kernel. However, functioning Operating System is not only the Kernel but also the Device Drivers, Bootloader, Applications and the bootstrap code.

All those elements must be qualified if will be used in the certified system.

QNX is not a magical box which will always be ASIL-D.

### 5.3 The Development Costs
There are always two of them: License for stuff and The Support Plan.

Basic set of licenses for project is:

* OS SDP (Software Development Platforms) License
* OS Technical Support Plan subscription
* OS Safety Products License
* OS Safety Technical Support Plan subscription

Also:

* License to use QNX OS in your project is paid separately, once per project.

## 6 vxWorks family
VxWorks is a real-time operating system (RTOS) developed as proprietary software by Wind River Systems, a wholly owned subsidiary of TPG Capital, US. First released in 1987, VxWorks is designed for use in embedded systems requiring real-time, deterministic performance and, in many cases, safety and security certification, for industries, such as aerospace and defense, medical devices, industrial equipment, robotics, energy, transportation, network infrastructure, automotive, and consumer electronics.

<https://en.wikipedia.org/wiki/VxWorks>

<https://www.windriver.com/products/vxworks>

VxWorks supports AMD/Intel architecture, POWER architecture, ARM architectures and RISC-V. The RTOS can be used in multicore asymmetric multiprocessing (AMP), symmetric multiprocessing (SMP), and mixed modes and multi-OS (via Type 1 hypervisor) designs on 32- and 64-bit processors.

VxWorks comes with the kernel, middleware, board support packages, Wind River Workbench development suite and complementary third-party software and hardware technologies. In its latest release, VxWorks 7, the RTOS has been re-engineered for modularity and upgradeability, so the OS kernel is separate from middleware, applications and other packages.  Scalability, security, safety, connectivity, and graphics have been improved to address Internet of Things (IoT) needs.

VxWorks key milestones are:

* 1987: Initial Release
* 1980s: VxWorks adds support for 32-bit processors.
* 1990s: VxWorks 5 supports a networking stack.
* 2004: VxWorks 6 release adds RTPs
* 2000s: VxWorks 6 supports SMP and adds derivative industry-specific platforms.
* 2014: VxWorks 7 Release adds support for 64-bit processing.

vxWorks supports POSIX and is an UNIX-like system, sort of…

### 6.1 Real Time Processes
Prior to VxWorks 6.0, the operating system provided a single memory space with no segregation of the operating system from user applications. All tasks ran in supervisor mode. Although this model afforded performance and flexibility when developing applications, only skilled programming could ensure that kernel facilities and applications coexisted in the same memory space without interfering with one another. 

With the release of VxWorks 6.0, the operating system provides support for real-time processes (RTPs) that includes execution of applications in user mode and other features common to operating systems with a clear delineation between kernel and applications. This architecture is often referred to as the process model. VxWorks has adopted this model with a design specifically aimed to meet the requirements of determinism and speed that are required for hard real-time systems. VxWorks 6.x provides full MMU-based protection of both kernel and user space. 

<https://learning.windriver.com/real-time-processes>

![](img/rtp_memory.png)

### 6.2 Kernel Shell
The VxWorks kernel shell is a target-resident shell that can be accessed locally from a host system over a serial connection using a terminal utility (such as tip  or HyperTerminal ), remotely over a network connection (using telnet , rlogin), or using the wtxConsole .

The VxWorks kernel shell provides a shell interface that can be used to execute, monitor, and debug kernel and RTP code, as well as to monitor and debug general system performance. For most uses, the kernel shell requires the system symbol table. It can also be used with the kernel object module loader to download object code into kernel space (for information in this regard, see 3.Kernel Object-Module Loader ).

The kernel shell has two interpreters - the C interpreter and the command Interpreter-each of which has its own set of commands. The C interpreter can also be used to execute any VxWorks kernel routines, or any routines that have been added to kernel space with downloadable kernel modules. The command interpreter, on the other hand, is similar to a UNIX shell. In contrast with the C interpreter (for which all commands are C system functions), the command interpreter commands are statically registered. This means that the command interpreter can be used without the system symbol table.

For the most part, the target-resident kernel shell works the same as the host shell, except that it executes entirely in the target system. In addition, the kernel shell only provides the C interpreter and command interpreter (others may be registered by the user), whereas the host shell provides additional facilities.

<https://learning.windriver.com/vxworks-kernel-shell>

![](img/VxWorks_7_Bootup_Screen.png)

## 7 Virtualization Basic Concepts
Virtualization creates a simulated, or virtual, computing environment as opposed to a physical environment. Virtualization often includes computer-generated versions of hardware, operating systems, storage devices, and more. This allows organizations to partition a single physical computer or server into several virtual machines. Each virtual machine can then interact independently and run different operating systems or applications while sharing the resources of a single host machine.

Emulation refers to the ability of a computer program in an electronic device to emulate (or imitate) another program or device.

In computing, an emulator is hardware or software that enables one computer system (called the host) to behave like another computer system (called the guest). An emulator typically enables the host system to run software or use peripheral devices designed for the guest system.

<https://en.wikipedia.org/wiki/Virtualization>

<https://en.wikipedia.org/wiki/Emulator>

### 7.1 Virtual Machine
A virtual machine (VM) is the virtualization/emulation of a computer system. Virtual machines are based on computer architectures and provide functionality of a physical computer. Their implementations may involve specialized hardware, software, or a combination. Virtual machines differ and are organized by their function, shown here:     

* System virtual machines (also termed full virtualization VMs) provide a substitute for a real machine. They provide functionality needed to execute entire operating systems. A hypervisor uses native execution to share and manage hardware, allowing for multiple environments which are isolated from one another, yet exist on the same physical machine. Modern hypervisors use hardware-assisted virtualization, virtualization-specific hardware, primarily from the host CPUs.
* Process virtual machines are designed to execute computer programs in a platform-independent environment.

<https://en.wikipedia.org/wiki/Virtual_machine>

### 7.2 OS-level Virtualization (containers)
OS-level virtualization is an operating system paradigm in which the kernel allows the existence of multiple isolated user space instances. Such instances, called containers (LXC, Solaris containers, Docker), Zones (Solaris containers), virtual private servers (OpenVZ), partitions (QNX), virtual environments (VEs), virtual kernels (DragonFly BSD), or jails (FreeBSD jail or chroot jail), may look like real computers from the point of view of programs running in them. A computer program running on an ordinary operating system can see all resources (connected devices, files and folders, network shares, CPU power, quantifiable hardware capabilities) of that computer. However, programs running inside of a container can only see the container's contents and devices assigned to the container.

On Unix-like operating systems, this feature can be seen as an advanced implementation of the standard chroot mechanism, which changes the apparent root folder for the current running process and its children. In addition to isolation mechanisms, the kernel often provides resource-management features to limit the impact of one container's activities on other containers.

Docker is a set of platform as a service (PaaS) products that use OS-level virtualization to deliver software in packages called containers. Containers are isolated from one another and bundle their own software, libraries and configuration files; they can communicate with each other through well-defined channels. Because all the containers share the services of a single operating system kernel, they use fewer resources than virtual machines.

![](img/vm_vs_docker.png)

Alternative to docker is Adaptive partitioning. It can be found in the QNX and vxWorks. It was introduced to protect different applications or groups of applications from others. You don't want one application—whether defective or malicious—to corrupt another or prevent it from running.

<https://docs.docker.com/>

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.neutrino.sys_arch/topic/adaptive.html>

### 7.3 Hardware Virtualization
Hardware virtualization is the virtualization of computers as complete hardware platforms, certain logical abstractions of their componentry, or only the functionality required to run various operating systems. Virtualization hides the physical characteristics of a computing platform from the users, presenting instead an abstract computing platform. At its origins, the software that controlled virtualization was called a "control program", but the terms "hypervisor" or "virtual machine monitor" became preferred over time.

From the most to the less overhead approach:

* Emulation
* Hardware Virtualization
* OS-level Virtualization

#### 7.3.1 SLAT
Second Level Address Translation (SLAT), also known as nested paging, is a hardware-assisted virtualization technology which makes it possible to avoid the overhead associated with software-managed shadow page tables.

AMD has supported SLAT through the Rapid Virtualization Indexing (RVI) technology since the introduction of its third-generation Opteron processors (code name Barcelona). Intel's implementation of SLAT, known as Extended Page Table (EPT), was introduced in the Nehalem microarchitecture found in certain Core i7, Core i5, and Core i3 processors.

ARM's virtualization extensions support SLAT, known as Stage-2 page-tables provided by a Stage-2 MMU. The guest uses the Stage-1 MMU. Support was added as optional in the ARMv7ve architecture and is also supported in the ARMv8 (32-bit and 64-bit) architectures.

AMD developed its first-generation virtualization extensions under the code name "Pacifica", and initially published them as AMD Secure Virtual Machine (SVM), but later marketed them under the trademark AMD Virtualization, abbreviated AMD-V.

AMD Opteron CPUs beginning with the Family 0x10 Barcelona line, and Phenom II CPUs, support a second-generation hardware virtualization technology called Rapid Virtualization Indexing (formerly known as Nested Page Tables during its development), later adopted by Intel as Extended Page Tables (EPT).

The CPU flag for AMD-V is "svm". This may be checked in BSD derivatives via dmesg or sysctl and in Linux via /proc/cpuinfo. Instructions in AMD-V include VMRUN, VMLOAD, VMSAVE, CLGI, VMMCALL, INVLPGA, SKINIT, and STGI.

Previously codenamed "Vanderpool", VT-x represents Intel's technology for virtualization on the x86 platform. The CPU flag for VT-x capability is "vmx"; in Linux, this can be checked via /proc/cpuinfo.

"VMX" stands for Virtual Machine Extensions, which adds 13 new instructions: VMPTRLD, VMPTRST, VMCLEAR, VMREAD, VMWRITE, VMCALL, VMLAUNCH, VMRESUME, VMXOFF, VMXON, INVEPT, INVVPID, and VMFUNC. These instructions permit entering and exiting a virtual execution mode where the guest OS perceives itself as running with full privilege (ring 0), but the host OS remains protected.

Intel started to include Extended Page Tables (EPT), a technology for page-table virtualization, since the Nehalem architecture, released in 2008.

#### 7.3.2 Ring -1
Intel’s Virtualization Technology (VT-x) (e.g.IntelXeon) and AMD’s AMD-V both target privileged instructions with a new CPU execution mode feature that allows the VMM to run in a new root mode below ring 0, also referred to as Ring 0P (for privileged root mode) while the Guest OS runs in Ring 0D (for de-privileged non-root mode).

<https://en.wikipedia.org/wiki/X86_virtualization#Intel-VT-x>

<https://en.wikipedia.org/wiki/X86_virtualization#AMD_virtualization_(AMD-V)>

Privileged and sensitive calls are set to automatically trap to the hypervisor and handled by hardware, removing the need for either binary translation or para-virtualization.

#### 7.3.3  ARM Virtualization Extension
The ARM v7-A and ARM v8-A architectures include optional virtualization extensions that allow a hypervisor to manage fully hardware virtualized guests.

Extension introduces the new Exception Level 2 which is devoted to running the Hypervisor in it.

<https://developer.arm.com/documentation/ddi0406/c/System-Level-Architecture/The-System-Level-Programmers--Model/The-Virtualization-Extensions>

### 7.4 Para vs Full Virtualization
Full virtualization requires that every salient feature of the hardware be reflected into one of several virtual machines – including the full instruction set, input/output operations, interrupts, memory access, and whatever other elements are used by the software that runs on the bare machine, and that is intended to run in a virtual machine. In such an environment, any software capable of execution on the raw hardware can be run in the virtual machine and, in particular, any operating systems. The obvious test of full virtualization is whether an operating system intended for stand-alone use can successfully run inside a virtual machine.

In computing, para-virtualization is a virtualization technique that presents a software interface to the virtual machines which is similar, yet not identical to the underlying hardware–software interface.

The intent of the modified interface is to reduce the portion of the guest's execution time spent performing operations which are substantially more difficult to run in a virtual environment compared to a non-virtualized environment. The paravirtualization provides specially defined 'hooks' to allow the guest(s) and host to request and acknowledge these tasks, which would otherwise be executed in the virtual domain (where execution performance is worse). A successful paravirtualized platform may allow the hypervisor to be simpler (by relocating execution of critical tasks from the virtual domain to the host domain), and/or reduce the overall performance degradation of machine execution inside the virtual guest.

In paravirtualization, the guest operating system is not only aware that it is running on a hypervisor but includes code to make guest-to-hypervisor transitions more efficient.

Usually, the mix of both approaches is used.

![](img/para_vs_full.png)

## 8 Hypervisors
A hypervisor (or Virtual Machine Monitor) is computer software, firmware or hardware that creates and runs virtual machines. A computer on which a hypervisor runs one or more virtual machines is called a host machine, and each virtual machine is called a guest machine. The hypervisor presents the guest operating systems with a virtual operating platform and manages the execution of the guest operating systems. Multiple instances of a variety of operating systems may share the virtualized hardware resources: for example, Linux, Windows, and macOS instances can all run on a single physical machine. This contrasts with operating-system-level virtualization, where all instances (usually called containers) must share a single kernel, though the guest operating systems can differ in user space, such as different Linux distributions with the same kernel.

Type-1, native or bare-metal hypervisors These hypervisors run directly on the host's hardware to control the hardware and to manage guest operating systems. For this reason, they are sometimes called bare metal hypervisors. Modern type-1 hypervisors include AntsleOS, Microsoft Hyper-V and Xbox One system software, Nutanix AHV, XCP-ng, Oracle VM Server for SPARC, Oracle VM Server for x86, POWER Hypervisor and VMware ESXi (formerly ESX), Xen, QNX Hypervisor for Safety and Wind River Helix.

Type-2 or hosted hypervisors These hypervisors run on a conventional operating system (OS) just as other computer programs do. A guest operating system runs as a process on the host. Type-2 hypervisors abstract guest operating systems from the host operating system. Parallels Desktop for Mac, QEMU, VirtualBox, VMware Player and VMware Workstation are examples of type-2 hypervisors.

![](img/hypervisor_types.png)

### 8.1 Linux Based Solutions

#### 8.1.1 KVM
Kernel-based Virtual Machine (KVM) is a virtualization module in the Linux kernel that allows the kernel to function as a hypervisor. It was merged into the Linux kernel mainline in kernel version 2.6.20, which was released on February 5, 2007. KVM requires a processor with hardware virtualization extensions, such as Intel VT or AMD-V. KVM has also been ported to other operating systems such as FreeBSD and illumos in the form of loadable kernel modules.

<https://en.wikipedia.org/wiki/Kernel-based_Virtual_Machine>

<https://www.linux-kvm.org/page/Main_Page>

KVM was originally designed for x86 processors but has since been ported to S/390, PowerPC, IA-64, and ARM.

KVM provides hardware-assisted virtualization for a wide variety of guest operating systems including Linux, BSD, Solaris, Windows, Haiku, ReactOS, Plan 9, AROS Research Operating System and macOS. In addition, Android 2.2, GNU/Hurd (Debian K16), Minix 3.1.2a, Solaris 10 U3 and Darwin 8.0.1, together with other operating systems and some newer versions of these listed, are known to work with certain limitations.

![](img/Kernel-based_Virtual_Machine.svg)

KVM provides device abstraction but no processor emulation. It exposes the /dev/kvm interface, which a user mode host can then use to:

Set up the guest VM's address space. The host must also supply a firmware image (usually a custom BIOS when emulating PCs) that the guest can use to bootstrap into its main OS.

Feed the guest simulated I/O.

Map the guest's video display back onto the system host.

On Linux, QEMU versions 0.10.1 and later is one such userspace host. QEMU uses KVM when available to virtualize guests at near-native speeds, but otherwise falls back to software-only emulation.

#### 8.1.2 XEN Hypervisor
Xen is a type-1 virtual machine, providing services that allow multiple computer operating systems to execute on the same computer hardware concurrently. It was originally developed by the University of Cambridge Computer Laboratory and is now being developed by the Linux Foundation with support from Intel.

<https://en.wikipedia.org/wiki/Xen>

<https://xenproject.org/users/why-xen/>

The Xen Project community develops and maintains Xen Project as free and open-source software, subject to the requirements of the GNU General Public License (GPL), version 2. Xen Project is currently available for the IA-32, x86-64 and ARM instruction sets.
##### 8.1.2.1 Dom0 and DomU
Dom0 is the initial domain started by the Xen hypervisor on boot. Dom0 is an abbreviation of "Domain 0" (sometimes written as "domain zero" or the "host domain"). Dom0 is a privileged domain that starts first and manages the DomU unprivileged domains.

The Xen hypervisor is not usable without Dom0. This is essentially the "host" operating system (or a "service console", if you prefer). As a result, Dom0 runs the Xen management tool stack, and has special privileges, like being able to access the hardware directly.

Dom0 has drivers for hardware, and it provides Xen virtual disks and network access for guests each referred to as a domU (unprivileged domains). For hardware that is made available to other domains, like network interfaces and disks, it will run the Backend Driver, which multiplexes and forwards to the hardware requests from the Frontend Driver in each DomU.

A DomU is the counterpart to Dom0; it is an unprivileged domain with (by default) no access to the hardware. It must run a Frontend Driver for multiplexed hardware it wishes to share with other domains.

![](img/A-simplified-architecture-of-Xen-All-I-O-requests-are-passed-from-DomU-to-Dom0-for.png)

### 8.2 QNX Hypervisor
The QNX Hypervisor 2.0 comprises the hypervisor microkernel and one or more instances of the qvm process.

![](img/qvm_arch.png)

It is similar to the KVM approach in which the hypervisor is fully functional Linux OS.

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.hypervisor.nav/topic/bookset.html>

#### 8.2.1 QVM
In a QNX hypervisor environment, a VM is implemented in a qvm process instance. The qvm process is an OS process that runs in the hypervisor host, outside the kernel. Each instance has an identifier marking it so that the microkernel knows that it is a qvm process.

When you configure a qvm, you are assembling a hardware platform. The difference is that instead of assembling physical memory cards, CPUs, etc., you specify the virtual components of your machine, which a qvm process will create and configure according to your specifications

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.hypervisor.user/topic/virt/qvm.html>

#### 8.2.2 Memory Access
A guest in a QNX virtualized environment can use following memory types:

* Virtualized memory
* Pass-through memory
* Shared memory

<http://www.qnx.com/developers/docs/7.1/#com.qnx.doc.hypervisor.user/topic/virt/mem.html>

In a QNX virtualized environment, a guest configured with 1 GB of RAM will see 1 GB available to it, just as it would see the RAM available to it if it were running in a non-virtualized environment. This memory allocation appears to the guest as physical memory. It is in fact memory assembled by the virtualization configuration from discontiguous physical memory. ARM calls this assembled memory intermediate physical memory; Intel calls it guest physical memory.

Guests accessing physical devices as pass-through devices need to map these devices into the memory regions configured to be accessible to them. Note that the guest-physical address for a pass-through device will not necessarily be the same address as the host-physical address in the hypervisor domain. In other words, there is no correspondence between the physical address seen by the guest, and the physical address seen from the hypervisor host domain.

Portions of physical memory can be allocated to be shared between guests. Guests will use a virtual device such as vdev-shmem to attach to the same physical address (PA) and use the shared memory area to share data, triggering each other whenever new data is available or has been read.

In a hypervisor system, a pass-through device is a physical device to which a guest has direct and exclusive access. Because it is direct and exclusive, this type of access to a physical device may be faster than access through a virtual device, or than when access is shared with other guests or the hypervisor host.
To use a pass-through device, the guest must have its own driver for the physical device. No vdev is required in the qvm process hosting the guest, and no driver is required in the hypervisor itself.
With a pass-through device, the hypervisor host domain only knows that it must route interrupts from the physical device directly to the guest, and pass signals from the guest directly to the device. All interaction is between the guest and the device; the hypervisor’s only responsibility is to identify and allow to pass through the interrupts from the device, and the signals from the guest.


### 8.3 Wind River Helix
Wind River Helix Virtualization Platform is a commercial off-the shelf (COTS) product for delivering an automotive solution that enables application consolidation with different levels of safety criticality on a single edge compute platform. This virtualization platform supports mixed-criticality OSes, providing the ability to run safety-critical and general-purpose applications side by side. 

The Helix Platform is a type 1 hypervisor.

Architecture is similar to the XEN hypervisor with the VxWorks OS as the Dom0.

![](img/helix_structure.png)

#### 8.3.1 Basic Helix System Elements
The Hypervisor provides:

* Managing and scheduling guest contexts
* Separating each VM in the system
* Resource managment

The hypervisor relies on a dedicated VM, root OS, for other tasks, such as:

* Access to storage
* Configuration
* Device virtualziation
* Booting and enabling device drivers

Root OS provides:

* HVCONFIG, the primary run-time interface to interact with the hypervisor configuration system.
* The hypervisor C API, a compile-time interface to the hypervisor configuration system.
* Persistent file storage for the system (e.g., ROMFS, network, USB)
* Console I/O for the hypervisor
* Device drivers that shared guest device need (e.g., VIRTIO)

* Root OS has full access to the entire physical memory, and all of the physical devices
* This lets the root OS manage memory used by the hypervisor to create the VMs.
* The hypervisor and the root OS execute from the same memory space

#### 8.3.2 Hypervisor Provided Virtual Devices
Virtual programmable interrupt controller (PIC) and counters.

Static mapping of phisical interrupts to the VMs (no shared phisical interrupts between VMs)

Virtual interrupts

Inter-VM interrupts

Interrupt queuing (interrupts are not lost when a guest is not scheduled)

Virtual clock source

Virtual peripherial component interconneciton (PCI) bus with separate virtual PCI controllers.

Bus can be used mulitple guests that will not see devices not assigned to them.


## 9 CyberSecurity
Computer security, cybersecurity or information technology security (IT security) is the protection of computer systems and networks from information disclosure, theft of or damage to their hardware, software, or electronic data, as well as from the disruption or misdirection of the services they provide.

### 9.1 Basic Concepts
#### 9.1.1 Cryptography
Cryptography, or cryptology, is the practice and study of techniques for secure communication in the presence of third parties called adversaries. More generally, cryptography is about constructing and analyzing protocols that prevent third parties or the public from reading private messages; various aspects in information security such as:

* data confidentiality,
* data integrity,
* authentication,
* non-repudiation

are central to modern cryptography.

#### 9.1.2 Key
A cryptographic key is a string of data that is used to lock or unlock cryptographic functions, including authentication, authorization and encryption. Cryptographic keys are grouped into cryptographic key types according to the functions they perform.

Key can be referred to as secret.

<https://en.wikipedia.org/wiki/Key_(cryptography)>

#### 9.1.3 Symmetric Encryption
Symmetric-key cryptography refers to encryption methods in which both the sender and receiver share the same key (or, less commonly, in which their keys are different, but related in an easily computable way). This was the only kind of encryption publicly known until June 1976.

<https://en.wikipedia.org/wiki/Symmetric-key_algorithm>

Symmetric key ciphers are implemented as either block ciphers or stream ciphers. A block cipher enciphers input in blocks of plaintext as opposed to individual characters, the input form used by a stream cipher.

The Data Encryption Standard (DES) and the Advanced Encryption Standard (AES) are block cipher designs that have been designated cryptography standards by the US government (though DES's designation was finally withdrawn after the AES was adopted).

<https://en.wikipedia.org/wiki/Data_Encryption_Standard>

The Advanced Encryption Standard (AES), is a specification for the encryption of electronic data established by the U.S. National Institute of Standards and Technology (NIST) in 2001.

<https://en.wikipedia.org/wiki/Advanced_Encryption_Standard>

It is most commonly used symetric encryption primitive. 

#### 9.1.3 Asymmetric Encryption
Public-key cryptography, or asymmetric cryptography, is a cryptographic system which uses pairs of keys: public keys (which may be known to others), and private keys (which may never be known by any except the owner). The generation of such key pairs depends on cryptographic algorithms which are based on mathematical problems termed one-way functions. Effective security requires keeping the private key private; the public key can be openly distributed without compromising security.

In such a system, any person can encrypt a message using the intended receiver's public key, but that encrypted message can only be decrypted with the receiver's private key. This allows, for instance, a server program to generate a cryptographic key intended for a suitable symmetric-key cryptography, then to use a client's openly-shared public key to encrypt that newly generated symmetric key.

<https://en.wikipedia.org/wiki/Public-key_cryptography>

Compared to symmetric encryption, asymmetric encryption is rather slower than good symmetric encryption, too slow for many purposes. Today's cryptosystems (such as TLS, Secure Shell) use both symmetric encryption and asymmetric encryption.

RSA (Rivest–Shamir–Adleman) is a public-key cryptosystem that is widely used for secure data transmission. It is also one of the oldest.

<https://en.wikipedia.org/wiki/RSA_(cryptosystem)>

RSA is a relatively slow algorithm. Because of this, it is not commonly used to directly encrypt user data. More often, RSA is used to transmit shared keys for symmetric key cryptography, which are then used for bulk encryption-decryption.

Elliptic-curve cryptography (ECC) is an approach to public-key cryptography based on the algebraic structure of elliptic curves over finite fields. ECC allows smaller keys compared to non-EC cryptography (based on plain Galois fields) to provide equivalent security.

<https://en.wikipedia.org/wiki/Elliptic-curve_cryptography>

#### 9.1.4 Shared Secret
A shared secret is a piece of data, known only to the parties involved, in a secure communication. This usually refers to the key of a symmetric cryptosystem. The shared secret can be a password, a passphrase, a big number, or an array of randomly chosen bytes.

<https://en.wikipedia.org/wiki/Shared_secret>

<https://en.wikipedia.org/wiki/Diffie%E2%80%93Hellman_key_exchange>

The shared secret is either shared beforehand between the communicating parties, in which case it can also be called a pre-shared key, or it is created at the start of the communication session by using a key-agreement protocol, for instance using public-key cryptography such as Diffie–Hellman or using symmetric-key cryptography such as Kerberos.

The shared secret can be used for authentication (for instance when logging into a remote system) using methods such as challenge–response or it can be fed to a key derivation function to produce one or more keys to use for encryption and/or MACing of messages.

To make unique session and message keys the shared secret is usually combined with an initialization vector (IV). An example of this is the derived unique key per transaction method.

All private keys must be protected.  All public keys must be authenticated.

#### 9.1.5 FIPS
The United States' Federal Information Processing Standards (FIPS) are publicly announced standards developed by the National Institute of Standards and Technology for use in computer systems by non-military American government agencies and government contractors.

<https://en.wikipedia.org/wiki/Federal_Information_Processing_Standards>

<https://en.wikipedia.org/wiki/FIPS_140>

FIPS standards are issued to establish requirements for various purposes such as ensuring computer security and interoperability and are intended for cases in which suitable industry standards do not already exist. Many FIPS specifications are modified versions of standards used in the technical communities, such as the American National Standards Institute (ANSI), the Institute of Electrical and Electronics Engineers (IEEE), and the International Organization for Standardization (ISO).

Some cipher algorithms like Data Encryption Standard (FIPS 46-3) and the Advanced Encryption Standard (FIPS 197) are covered by FIPS.

The National Institute of Standards and Technology (NIST) issues the 140 Publication Series to coordinate the requirements and standards for cryptographic modules which include both hardware and software components for use by departments and agencies of the United States federal government. FIPS 140 does not purport to provide sufficient conditions to guarantee that a module conforming to its requirements is secure, still less that a system built using such modules is secure. The requirements cover not only the cryptographic modules themselves but also their documentation and (at the highest security level) some aspects of the comments contained in the source code.

User agencies desiring to implement cryptographic modules should confirm that the module they are using is covered by an existing validation certificate. FIPS 140-1 and FIPS 140-2 validation certificates specify the exact module name, hardware, software, firmware, and/or applet version numbers. For Levels 2 and higher, the operating platform upon which the validation is applicable is also listed. Vendors do not always maintain their baseline validations.

#### 9.1.6 Cryptographic Hash Function
A cryptographic hash function (CHF) is a mathematical algorithm that maps data of arbitrary size (often called the "message") to a bit array of a fixed size (the "hash value", "hash", or "message digest"). It is a one-way function, that is, a function which is practically infeasible to invert. Ideally, the only way to find a message that produces a given hash is to attempt a brute-force search of possible inputs to see if they produce a match, or use a rainbow table of matched hashes. Cryptographic hash functions are a basic tool of modern cryptography.

<https://en.wikipedia.org/wiki/Cryptographic_hash_function>

<https://en.wikipedia.org/wiki/SHA-2>

The ideal cryptographic hash function has the following main properties:

1. it is deterministic, meaning that the same message always results in the same hash
2. it is quick to compute the hash value for any given message
3. it is infeasible to generate a message that yields a given hash value (i.e. to reverse the process that generated the given hash value)
4. it is infeasible to find two different messages with the same hash value
5. a small change to a message should change the hash value so extensively that a new hash value appears uncorrelated with the old hash value (avalanche effect)

#### 9.1.7 Key Derivvation Function
A key derivation function (KDF) is a cryptographic hash function that derives one or more secret keys from a secret value such as a main key, a password, or a passphrase using a pseudorandom function. KDFs can be used to stretch keys into longer keys or to obtain keys of a required format, such as converting a group element that is the result of a Diffie–Hellman key exchange into a symmetric key for use with AES. Keyed cryptographic hash functions are popular examples of pseudorandom functions used for key derivation.

<https://en.wikipedia.org/wiki/Key_derivation_function>

#### 9.1.8 Digital Signature
A digital signature is a mathematical scheme for verifying the authenticity of digital messages or documents. A valid digital signature, where the prerequisites are satisfied, gives a recipient very strong reason to believe that the message was created by a known sender (authentication), and that the message was not altered in transit (integrity).

<https://en.wikipedia.org/wiki/Digital_signature>

Digital signatures are a standard element of most cryptographic protocol suites, and are commonly used for software distribution, financial transactions, contract management software, and in other cases where it is important to detect forgery or tampering.

#### 9.1.9 Message Authentication Code
Message authentication code (MAC), sometimes known as a tag, is a short piece of information used to authenticate a message, in other words, to confirm that the message came from the stated sender (its authenticity) and has not been changed. The MAC value protects a message's data integrity, as well as its authenticity, by allowing verifiers (who also possess the secret key) to detect any changes to the message content.

<https://en.wikipedia.org/wiki/Message_authentication_code>

### 9.2 Trusted Execution Environment vs HSM
A trusted execution environment (TEE) is a secure area of a main processor. It guarantees code and data loaded inside to be protected with respect to confidentiality and integrity. A TEE as an isolated execution environment provides security features such as isolated execution, integrity of applications executing with the TEE, along with confidentiality of their assets. In general terms, the TEE offers an execution space that provides a higher level of security for trusted applications running on the device than a rich operating system (OS) and more functionality than a 'secure element' (SE).

<https://en.wikipedia.org/wiki/Trusted_execution_environment>

<https://en.wikipedia.org/wiki/ARM_architecture#TrustZone>

<https://www.arm.com/technologies/trustzone-for-cortex-a>

The fundamental concepts of a TEE are trust, security and isolation of sensitive data. The most advanced TEE implementations embed devices with unique identities via roots of trust. These enable key stakeholders in the value chain to identify whether the device they’re interacting with is authentic. It also cryptographically protects both data and applications stored inside it. Applications that sit within the TEE are known as trusted applications. The data stored on and processed by trusted applications is protected and interactions made (whether between applications or the device and end user) are securely executed.

TEE allows:

1. Secure peripheral access — It has the unique capability of being able to directly access and secure peripherals such as the touchscreen or display (i.e., the user interface), offering protection for fingerprint sensors, cameras, microphones, speakers and so on.
2. Secure communication with remote entities — It can secure data, communications and cryptographic operations. Encryption keys are only stored, managed and used within the secure environment, with no opportunity for eavesdropping. This is particularly relevant for IoT as secure cloud enrollment of things like sensors is central to scalability.
3. Trusted device identity and authentication — Some TEEs inject a root of trust that enables the legitimacy of the device to be verified by the connected service which it is trying to enroll with.

OP-TEE is a Trusted Execution Environment (TEE) designed as companion to a non-secure Linux kernel running on Arm; Cortex-A cores using the TrustZone technology. OP-TEE implements TEE Internal Core API v1.1.x which is the API exposed to Trusted Applications and the TEE Client API v1.0, which is the API describing how to communicate with a TEE. Those APIs are defined in the GlobalPlatform API specifications.

<https://www.op-tee.org/>

<https://optee.readthedocs.io/en/latest/>

A hardware security module (HSM) is a physical computing device that safeguards and manages digital keys, performs encryption and decryption functions for digital signatures, strong authentication and other cryptographic functions. These modules traditionally come in the form of a plug-in card or an external device that attaches directly to a computer or network server. A hardware security module contains one or more secure cryptoprocessor chips.

Different standardized feature sets for HTAs

* Secure Hardware Extension (SHE)
* Hardware Security Module (HSM)
* Trusted Platform Module (TPM)

Different brand names for HTA by different HW suppliers

* Infineon: Aurix HSM / SHE+ driver
* Renesas: Intelligent Cryptographic Unit (ICU)
* Freescale: Crypto Service Engine (CSE)
* ARM: Trust Zone

### 9.3 Intruder Detection System
An intrusion detection system (IDS) is a device or software application that monitors a network or systems for malicious activity or policy violations. Any intrusion activity or violation is typically reported either to an administrator or collected centrally using a security information and event management (SIEM) system. A SIEM system combines outputs from multiple sources and uses alarm filtering techniques to distinguish malicious activity from false alarms.

<https://en.wikipedia.org/wiki/Intrusion_detection_system>

<https://www.barracuda.com/glossary/intrusion-detection-system>

Network intrusion detection systems (NIDS) are placed at a strategic point or points within the network to monitor traffic to and from all devices on the network. It performs an analysis of passing traffic on the entire subnet and matches the traffic that is passed on the subnets to the library of known attacks. Once an attack is identified, or abnormal behavior is sensed, the alert can be sent to the administrator. An example of a NIDS would be installing it on the subnet where firewalls are in order to see if someone is trying to break into the firewall. Ideally one would scan all inbound and outbound traffic, however doing so might create a bottleneck that would impair the overall speed of the network.

![](img/nids.png)

NID Systems are also capable of comparing signatures for similar packets to link and drop harmful detected packets which have a signature matching the records in the NIDS. When we classify the design of the NIDS according to the system interactivity property, there are two types: on-line and off-line NIDS, often referred to as inline and tap mode, respectively. On-line NIDS deals with the network in real time. It analyses the Ethernet packets and applies some rules, to decide if it is an attack or not. Off-line NIDS deals with stored data and passes it through some processes to decide if it is an attack or not.

A host-based intrusion detection system (HIDS) is an intrusion detection system that is capable of monitoring and analyzing the internals of a computing system as well as the network packets on its network interfaces.

<https://en.wikipedia.org/wiki/Host-based_intrusion_detection_system>

![](img/hids.png)

Host intrusion detection systems (HIDS) run on individual hosts or devices on the network. A HIDS monitors the inbound and outbound packets from the device only and will alert the user or administrator if suspicious activity is detected. It takes a snapshot of existing system files and matches it to the previous snapshot. If the critical system files were modified or deleted, an alert is sent to the administrator to investigate. An example of HIDS usage can be seen on mission critical machines, which are not expected to change their configurations.

Due to performance impact, not all of the IDS tasks have to be always in use.

One can think of a HIDS as an agent that monitors whether anything or anyone, whether internal or external, has circumvented the system's security policy.

In all networks should be at least one SIEM dedicated node which communicate with all other IDS nodes, keeps track of their status and possible threads detected.

<https://en.wikipedia.org/wiki/Security_information_and_event_management>

![](img/siem.png)